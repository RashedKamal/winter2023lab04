import java.util.Scanner;
public class Toaster {
    private int numSlots;
    private int maxTemp;
    private String type;

    public void printType(String type){
        System.out.println("The toaster is of " + type + " type");
    }

    public void toastBread(){
        System.out.println("The toaster is toasting bread");
    }

    public void changeTemp(int maxTemp){
        int validTemp = validateChangeTemp(maxTemp);
        this.maxTemp = validTemp;
        System.out.println("The max temp is: " + validTemp);
        }
    
    public int validateChangeTemp(int input){
        Scanner reader = new Scanner(System.in);
        int count = 0; // only increments to 1 if input is valid to terminate the loop
        while (count != 1){
            if (input <= 0){
               System.out.println("Try Again!");
               input = reader.nextInt();
            }
            else{
                count++;
            }
        }
        return input;
    }

    public int getNumSlots(){
        return this.numSlots;
    }
    public int getMaxTemp(){
        return this.maxTemp;
    }
    public String getType(){
        return this.type;
    }

    public void setNumSlots(int newNumSlots){
        this.numSlots = newNumSlots;
     } 
    public void setMaxTemp(int newMaxTemp){
        this.maxTemp = newMaxTemp;
    }
    public void setType(String newType){
        this.type = newType;
    }

// Constructors
/*public Toaster(int numSlots, int maxTemp, String type){
    this.numSlots = numSlots;
    this.maxTemp = maxTemp;
    this.type = type;
} */
@Override
public String toString() {
    return "Toaster: numslots [" + this.numSlots + "], maxtemp [" + this.maxTemp + "], type [" + this.type + "]";
}
}