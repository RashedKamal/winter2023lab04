import java.util.Scanner;

public class ToasterStore {
    public static void main(String[] args) {
        Toaster[] toasters = new Toaster[4];
        Scanner reader = new Scanner(System.in);
        for (int i = 0; i < 4; i++) {
            Toaster toaster = new Toaster();
            System.out.println("Enter number of slots: ");
            toaster.setNumSlots(Integer.parseInt(reader.nextLine()));
            System.out.println("Enter maximum temperature value: ");
            toaster.setMaxTemp(Integer.parseInt(reader.nextLine()));
            System.out.println("Enter the type of toaster(popup, convection, oven): ");
            toaster.setType(reader.nextLine());
            System.out.println(toaster.getNumSlots());
            System.out.println(toaster.getMaxTemp());
            System.out.println(toaster.getType());
            toasters[i] = toaster;
        }
		        for (int i = 0; i < 4; i++) {
            System.out.println(toasters[i]);
        }
        System.out.println("Change last toaster's number of slots: ");
        toasters[3].setNumSlots(Integer.parseInt(reader.nextLine()));
        System.out.println("Change last toaster's max temperature: ");
        toasters[3].setMaxTemp(Integer.parseInt(reader.nextLine()));
        System.out.println("Change last toaster's type: ");
        toasters[3].setType(reader.nextLine());
        System.out.println(toasters[3].getNumSlots());
        System.out.println(toasters[3].getMaxTemp());
        System.out.println(toasters[3].getType());
    }
}
        /*
         * System.out.println("The last toaster's number of slots is " +
         * toasters[3].numSlots);
         * System.out.println("The last toaster's max temperature is " +
         * toasters[3].maxTemp);
         * System.out.println("The last toaster's type is " + toasters[3].type);
         * 
         * Toaster firstToaster = new Toaster();
         * firstToaster.printType(toasters[0].type);
         * firstToaster.toastBread();
         */

        // Toaster secondToaster = new Toaster();
        // secondToaster.changeTemp(toasters[1].maxTemp);

        /*
         * Toaster toaster = new Toaster();
         * System.out.println("Enter number of slots: ");
         * toaster.setNumSlots(Integer.parseInt(reader.nextLine()));
         * System.out.println("Enter maximum temperature value: ");
         * toaster.setMaxTemp(Integer.parseInt(reader.nextLine()));
         * System.out.println("Enter the type of toaster(popup, convection, oven): ");
         * toaster.setType(reader.nextLine());
         * System.out.println(toaster.getNumSlots());
         * System.out.println(toaster.getMaxTemp());
         * System.out.println(toaster.getType());
         */

        // Constructors
        /*
         * Toaster conToaster = new Toaster(Integer.parseInt(reader.nextLine()),
         * Integer.parseInt(reader.nextLine()), reader.nextLine());
         * System.out.println(conToaster.getNumSlots());
         * System.out.println(conToaster.getMaxTemp());
         * System.out.println(conToaster.getType());
         */

